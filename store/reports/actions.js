import axios from 'axios';

export default {
  async FETCH_VERIFIED_REPORTS({ commit }, { dateFrom, dateTo }) {

    const employee = JSON.parse(localStorage.employee);
    this.role = employee.roleParam;
    await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/reports/view_verified_personal_out`,
      data: {
        employee_id: employee.id,
        modules: employee.modules,
        request_date_from: dateFrom,
        request_date_to: dateTo
      },
      headers: {
        authorization: "Bearer " + employee.token
      }
    })
      .then(res => {
        commit('SET_VERIFIED_REPORTS', res.data);
      })
      .catch(e => {});
  }

}