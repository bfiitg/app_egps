import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader
import VueBarcode from '@xkeshi/vue-barcode'
import Vue from 'vue'
// import Vuetify, { VIcon, VToolbar, VButton, VNavigationDrawer, VSnackbar } from 'vuetify';
import VueCookies from "vue-cookies";
import Loading from 'vue-loading-overlay'
// Vue.use(Vuetify, {
//     components: {
//         VToolbar,
//         VIcon,
//         VButton,
//         VNavigationDrawer,
//         VSnackbar
//     },
//     iconfont: 'fa'
// });

Vue.use(VueBarcode)
Vue.use(Loading)
Vue.use(VueCookies);

VueCookies.install(Vue)