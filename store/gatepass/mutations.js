import { isArray } from 'util';
import moment from 'moment';

export default {
  SET_SCHEDULE(state, data) {
    state.sched = [];
    if (!isArray(data)) {
      state.sched = null;
    } else {
      let item = data
        .map(
          item =>
            (item = {
              id: item.id,
              request_type: item.request_type.name,
              request_time_out: moment(
                `${item.request_time_out} ${item.request_date}`
                , "hh:mm a").format('hh:mm a'),
              request_time_in: item.request_time_in
                ? moment(
                  `${item.request_time_in} ${item.request_date}`,
                  "hh:mm a").format('hh:mm a')
                : null,
              request_date: item.request_date,
              status: item.status,
              updater: item.updater ? `${item.updater.first_name} ${
                item.updater.last_name}` : null,
              approval_time: item.approval_time
                ? moment(
                  `${item.approval_time}`,
                  "hh:mm a").format('hh:mm a')
                : null,
              actual_time_out: item.actual_time_out
                ? moment(
                  `${item.actual_time_out}`,
                  "hh:mm a").format('hh:mm a')
                : null,
              has_time_in: item.request_type.has_time_in
            })
        )

      ///  Filter gate pass
      item = item.filter(item => ((item.status != 'Time In')))
      item = item.filter(item => ((!(item.request_type == 'Half Day' && item.status == "Time Out") && !(item.request_type == 'Undertime' && item.status == "Time Out"))))
      // .filter(item => item.status === 'Pending'); 
      if (item.length === 0) {
        state.sched = null;
      } else {
        state.sched = item;
      }
    }
  },
  // SET_APPROVED(state, data) {
  //   state.approvedSched = [];
  //   if (!isArray(data)) {
  //     state.approvedSched = [];
  //   } else {
  //     const item = (state.approvedSched = data
  //       .map(
  //         item =>
  //           (item = {
  //             request_type: item.request_type.name,
  //             request_time_out: moment(
  //               `${item.request_time_out} ${item.request_date}`
  //               , 'hh:mm a').format('hh:mm a'),
  //             request_time_in: item.request_time_in
  //               ? moment(
  //                 `${item.request_time_in} ${item.request_date}`
  //                 , "hh:mm a").format('hh:mm a')
  //               : null,
  //             request_date: item.request_date,
  //             status: item.status
  //           })
  //       )
  //       .filter(item => item.status === 'Approved' || item.status === 'Pre-Approved'));
  //     if (item.length === 0) {
  //       state.approvedSched = [];
  //     } else {
  //       state.approvedSched = item;
  //     }
  //   }
  // },
  CLEAR_SCHEDULE(state) {
    state.sched = null;
    state.approvedSched = [];
  }
};
