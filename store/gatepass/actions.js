import axios from 'axios';


export default {

    async GET_SCHEDULE_OUT({ commit }) {
        const employee = JSON.parse(localStorage.employee)
        const gatePassAccess = await employee.modules.find(gatepass => gatepass.description === "Gate Pass")
        const requestAccess = await gatePassAccess.actions.find(action => action.description === "Request gate pass")
        if (requestAccess) {
            await axios({
                method: "POST",
                url: `${this.$axios.defaults.baseURL}/employees/schedule_out`,
                data: {
                    employee_id: employee.id,
                    role: employee.role,
                    modules: employee.modules,
                },
                headers: {
                    authorization: "Bearer " + employee.token
                }
            }).then(res => {
                // commit('SET_APPROVED', res.data)
                commit('SET_SCHEDULE', res.data)

            }).catch(e => {
                commit('CLEAR_SCHEDULE')
            })
        }
    },

}