export default {
    GET_VERIFIED_REPORTS(state) {
        return state.FinalData;
    },
    GET_DEPARTMENTS(state) {
        return state.Departments;
    },
    GET_REQUEST_TYPES(state) {
        return state.Request_type;
    }
}