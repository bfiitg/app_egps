import moment from 'moment';

export default{
    SET_TIME_IN(state,res){
        if(Array.isArray(res.data)){
            res.data.forEach(gate_pass =>{
                const exist = state.listOfTimeIn.find(
                    gatepass => gatepass.id === gate_pass.id
                )

                if(!exist){
                    state.listOfTimeIn.push({
                        employee_id: gate_pass.employee_id,
                    img_profile_path: gate_pass.img_profile_path
                    ? `${this.$axios.defaults.baseURL}/${gate_pass.img_profile_path}`:'@/assets/user.svg',
                    gate_pass_id: gate_pass.id,
                    name: `${gate_pass.employee.first_name} ${gate_pass.employee.last_name}`,
                    gate_pass_type: gate_pass.request_type.name,
                    gate_pass_color: gate_pass.request_type.color
                    ? gate_pass.request_type.color
                    : "white",
                    time: gate_pass.request_time_in
                    ? moment(
                        `${gate_pass.request_time_out}`,
                        "h:mm a"
                      ).format("h:mm a") +
                      "-" +
                      moment(`${gate_pass.request_time_in}`, "h:mm a").format(
                        "h:mm a"
                      )
                    : moment(
                        `${gate_pass.request_time_out}`,
                        "h:mm a"
                      ).format("h:mm a")
                    })
                }
            })
        }else{
            if(res.data){
                
            state.emptyText = res.data
            }else{
                state.emptyText = res;
            }
            state.listOfTimeIn = []
        }

    },SET_TIME_OUT(state,res){
        if(Array.isArray(res.data)){
            res.data.forEach(gate_pass =>{
                const exist = state.listOfTimeOut.find(
                    gatepass => gatepass.id === gate_pass.id
                )

                if(!exist){
                    state.listOfTimeIn.push({
                        employee_id: gate_pass.employee_id,
                    img_profile_path: gate_pass.employee.img_profile_path
                    ? `${this.$axios.defaults.baseURL}/${gate_pass.employee.img_profile_path}`:'@/assets/user.svg',
                    gate_pass_id: gate_pass.id,
                    name: `${gate_pass.employee.first_name} ${gate_pass.employee.last_name}`,
                    gate_pass_type: gate_pass.request_type.name,
                    gate_pass_color: gate_pass.request_type.color
                    ? gate_pass.request_type.color
                    : "white",
                    time: gate_pass.request_time_in
                    ? moment(
                        `${gate_pass.request_time_out}`,
                        "h:mm a"
                      ).format("h:mm a") +
                      "-" +
                      moment(`${gate_pass.request_time_in}`, "h:mm a").format(
                        "h:mm a"
                      )
                    : moment(
                        `${gate_pass.request_time_out}`,
                        "h:mm a"
                      ).format("h:mm a")
                    })
                }
            })
        }else{
            if(res.data){
                
            state.emptyText = res.data
            }else{
                state.emptyText = res;
            }
            state.listOfTimeIn = []
        }

    }
}