export default function ({ redirect }) {
    let employee;
    if (localStorage.employee) {

        employee = JSON.parse(localStorage.employee)
        const sec = employee.roleParam === 'sec'
        if (sec) {
            redirect('/monitor-dashboard')
        }

        const mapAdmin = employee.modules.find(
            element => element.description === "admin"
        );

        if (localStorage.token && !mapAdmin) {
            redirect('/dashboard')
        }
    } else {
        redirect('/login')
    }
}