export default function ({ redirect }) {
    let employee;
    if (localStorage.employee) {
        employee = JSON.parse(localStorage.employee);
        const sec = employee.roleParam === 'sec'
        if (sec) {
            redirect('/monitor-dashboard')
        }
        const mapMonitor = employee.modules.find(element =>
            element.actions.find(
                gatepass => gatepass.description === "View approved gate pass"
            )
        );
        if (localStorage.token && !mapMonitor && !sec) {
            redirect('/dashboard');
        }
    } else {
        redirect('/login');
    }
}