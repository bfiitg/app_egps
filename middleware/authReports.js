export default function ({ redirect }) {

    let employee;
    if (localStorage.employee) {
        employee = JSON.parse(localStorage.employee)
        const sec = employee.roleParam === 'sec'
        if (sec) {
            redirect('/monitor-dashboard')
        }

        const mapReports = employee.modules.find(
            element => element.description === "Report"
        );
        if (localStorage.token && !mapReports && !sec) {
            redirect('/dashboard')
        }
    } else {
        redirect('/login')
    }

}