import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import LoginForm from '@/components/Login/LoginForm';
import BootstrapVue from 'bootstrap-vue';

const localVue = createLocalVue();
localVue.use(BootstrapVue)

describe('LoginForm ', () => {
   const wrapper = mount(LoginForm, {
      localVue,
   });

   wrapper.setData({ user: { id: "154151107", password: "emp" } })
   test('is a Vue Instance', () => {
      expect(wrapper.isVueInstance()).toBeTruthy();

   })
   test('has an ID of login-form-component', () => {
      expect(wrapper.attributes('id')).toBe('login-form-component')
   })

   test('has a valid ID and password', () => {
      expect(wrapper.vm.user.id).toBe("154151107")
      expect(wrapper.vm.user.password).toBe("emp")
   })
   test('ID and password is not valid', () => {
      expect(wrapper.vm.user.id).not.toBe('-12345')
      expect(wrapper.vm.user.password).not.toBe('emb')
   })
   test('Login button exist', () => {
      expect(wrapper.contains('button')).toBe(true)
   })

   test('input ID and password exist', () => {
      expect(wrapper.contains('input')).toBe(true)
   })
   test('Login has a Form tag', () => {
      expect(wrapper.contains('form')).toBe(true)
   })
   test('can click login button', () => {
      wrapper.find('button').trigger('click')
      expect(wrapper.contains('button')).toBe(true)
   })

   test('no error on login click', () => {
      wrapper.find('button').trigger('click')
      expect(wrapper.text()).not.toContain("Incorrect Password")
   })

})