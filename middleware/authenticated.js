
export default async function ({ redirect }) {
  
  let employee
  if (localStorage.employee) {
    employee = JSON.parse(localStorage.employee)
    const sec = employee.roleParam === 'sec'
    if (sec) {
      redirect('/monitor-dashboard')
    }

    if (localStorage.token && !sec) {
      redirect('/Dashboard')
    }

  }
  
}
