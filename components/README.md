# COMPONENTS

**This directory is not required, you can delete it if you  don't want to use it.**

## 🔳 Dashboard
> To change the content of the dashboard of the UI. you can go to `/Dashboard`.

## 👨‍💻 Employee Profile
> This includes the info of the employee from localStorage. you can go to `/EmployeeProfile`


## ⚠ Admin Dashboard
>To change the content of the UI. you can go to `/AdminDashboard`.

## 🔵 Loading Page
This folder is not necessary to configure. but it will serve as another purpose to add a loading page to your application.

## 💻 Login
Login Folder consist of Login and ChangePassword file.

## 📄 Reports
The Reports folder contains the employee reports assessed by the PG(People Group) and Accountant.

## ✉ Request Table
This folder holds the Pending/Approved/Disapproved Table component that has the same purpose.
