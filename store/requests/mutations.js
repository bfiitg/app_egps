import moment from 'moment';

export default {
  SET_PENDING_REQUESTS(state, data) {
    const statePending = state.pending.find(data => { });
    state.pending = [];
    state.pendingWithDeduction = [];
    if (Array.isArray(data.listGatePass)) {
      data.listGatePass.map(employee => {
        employee.gate_passes.map(gate_pass => {
          let timeout = moment(
            `${gate_pass.request_time_out} ${gate_pass.request_date}`,
            'hh:mm a'
          ).format('hh:mm a');

          let timein;
          if (gate_pass.request_time_in) {
            timein = moment(
              `${gate_pass.request_time_in} ${gate_pass.request_date}`,
              'hh:mm a'
            ).format('hh:mm a');
          }

          state.pending.push(
            (gate_pass.gate_pass = {
              id: gate_pass.id,
              name: {
                name: employee.last_name + ', ' + employee.first_name,
                id: employee.id,
                position: employee.position.name,
                profile: employee.img_profile_path,
                department: employee.position.team.department.name,
                payroll_group: employee.payroll_group,
                is_offsite: employee.is_offsite
              },
              request_type: gate_pass.request_type.name,
              reason: gate_pass.reason,
              time: timein ? timeout + ' - ' + timein : timeout,
              date: moment(gate_pass.request_date).format('MM/DD/YYYY'),
              department: employee.position
                ? employee.position.team
                  ? employee.position.team.department
                    ? employee.position.team.department.name
                    : ''
                  : ''
                : '',
              team: employee.position
                ? employee.position.team
                  ? employee.position.team.name
                  : ''
                : ''
            })
          );
        });
      });
    } else if (typeof data.listGatePass === 'object') {
      data.listGatePass.withoutDeductions.map(employee => {
        employee.gate_passes.map(gate_pass => {
          let timeout = moment(
            `${gate_pass.request_time_out} ${gate_pass.request_date}`,
            'hh:mm a'
          ).format('hh:mm a');

          let timein;
          if (gate_pass.request_time_in) {
            timein = moment(
              `${gate_pass.request_time_in} ${gate_pass.request_date}`,
              'hh:mm a'
            ).format('hh:mm a');
          }
          const filterByPreApproved = gate_pass.gate_pass_status_logs.filter(
            log => log.status === "Pre-Approved"
          );
          let getPreApproved;
          if (filterByPreApproved.length > 0) {
            getPreApproved = filterByPreApproved.reduce((prev, current) => {
              if (+current.id > +prev.id) {
                return current;
              } else {
                return prev;
              }
            });
          }

          state.pending.push(
            (gate_pass.gate_pass = {
              id: gate_pass.id,
              name: {
                name: employee.last_name + ', ' + employee.first_name,
                id: employee.id,
                position: employee.position.name,
                profile: employee.img_profile_path,
                department: employee.position.team.department.name,
                payroll_group: employee.payroll_group,
                is_offsite: employee.is_offsite
              },
              request_type: gate_pass.request_type.name,
              reason: gate_pass.reason,
              time: timein ? timeout + ' - ' + timein : timeout,
              date: moment(gate_pass.request_date).format('MM/DD/YYYY'),
              department: employee.position
                ? employee.position.team
                  ? employee.position.team.department
                    ? employee.position.team.department.name
                    : ''
                  : ''
                : '',
              team: employee.position
                ? employee.position.team
                  ? employee.position.team.name
                  : ''
                : '',
              preapproved_by: getPreApproved
                ? getPreApproved.employee
                  ? `${getPreApproved.employee.first_name} ${getPreApproved.employee.middle_name} ${getPreApproved.employee.last_name}`
                  : ""
                : "",
              status: gate_pass.status
            })
          );
        });
      });


      data.listGatePass.withDeductions.map(employee => {
        employee.gate_passes.map(gate_pass => {
          let timeout = moment(
            `${gate_pass.request_time_out} ${gate_pass.request_date}`,
            'hh:mm a'
          ).format('hh:mm a');

          let timein;
          if (gate_pass.request_time_in) {
            timein = moment(
              `${gate_pass.request_time_in} ${gate_pass.request_date}`,
              'hh:mm a'
            ).format('hh:mm a');
          }

          const filterByPreApproved = gate_pass.gate_pass_status_logs.filter(
            log => log.status === "Pre-Approved"
          );
          let getPreApproved;
          if (filterByPreApproved.length > 0) {
            getPreApproved = filterByPreApproved.reduce((prev, current) => {
              if (+current.id > +prev.id) {
                return current;
              } else {
                return prev;
              }
            });
          }


          state.pendingWithDeduction.push(
            (gate_pass.gate_pass = {
              id: gate_pass.id,
              name: {
                name: employee.last_name + ' ' + employee.first_name,
                id: employee.id,
                position: employee.position.name,
                profile: employee.img_profile_path,
                department: employee.position.team.department.name,
                payroll_group: employee.payroll_group,
                is_offsite: employee.is_offsite
              },
              request_type: gate_pass.request_type.name,
              reason: gate_pass.reason,
              time: timein ? timeout + ' - ' + timein : timeout,
              date: moment(gate_pass.request_date).format('MM/DD/YYYY'),
              department: employee.position
                ? employee.position.team
                  ? employee.position.team.department
                    ? employee.position.team.department.name
                    : ''
                  : ''
                : '',
              team: employee.position
                ? employee.position.team
                  ? employee.position.team.name
                  : ''
                : '',
              preapproved_by: getPreApproved
                ? getPreApproved.employee
                  ? `${getPreApproved.employee.first_name} ${getPreApproved.employee.middle_name} ${getPreApproved.employee.last_name}`
                  : ""
                : "",
              status: gate_pass.status
            })
          );
        });
      });
    }

    else {
      state.pending = [];
      state.pendingWithDeduction = [];
    }

    if (Array.isArray(data.listDepartments)) {
      data.listDepartments = data.listDepartments.filter(department => department.status === 'Active');
      state.departments = data.listDepartments.map(
        department => department.name
      );
    } else {
      state.departments = [];
    }

    if (Array.isArray(data.listRequestTypes)) {
      data.listRequestTypes = data.listRequestTypes.filter(
        requestType => requestType.status === 'Active'
      );

      state.requestTypes = data.listRequestTypes.map(
        requestType => requestType.name
      );
    } else {
      state.requestTypes = [];
    }
  },
  SET_PENDING_COUNT(state, data) {
    if (data.withoutDeductions && data.withDeductions) {
      state.pendingCount = data.withDeductions.length + data.withoutDeductions.length
    }
    else {
      if (Array.isArray(data)) {

        state.pendingCount = data.length
      }
    }
  },
  SET_APPROVED_REQUESTS(state, data) {
    state.approved = [];
    if (Array.isArray(data.listGatePass)) {
      data.listGatePass.map(employee =>
        employee.gate_passes.map(gate_pass => {
          let timeout = moment(
            `${gate_pass.request_time_out} ${gate_pass.request_date}`,
            'hh:mm a'
          ).format('hh:mm a');

          let timein;
          if (gate_pass.request_time_in) {
            timein = moment(
              `${gate_pass.request_time_in} ${gate_pass.request_date}`,
              'hh:mm a'
            ).format('hh:mm a');
          }

          const filterByPreApproved = gate_pass.gate_pass_status_logs.filter(
            log => log.status === 'Approved'
          );
          const getPreApproved = filterByPreApproved.reduce((prev, current) => {
            if (+current.id > +prev.id) {
              return current;
            } else {
              return prev;
            }
          });

          state.approved.push(
            (gate_pass.gate_pass = {
              id: gate_pass.id,
              name: {
                name: employee.last_name + ', ' + employee.first_name, id: employee.id,
                is_offsite: employee.is_offsite
              },
              request_type: gate_pass.request_type.name,
              reason: gate_pass.reason,
              time: timein ? timeout + ' - ' + timein : timeout,
              date: moment(gate_pass.request_date).format('MM/DD/YYYY'),
              approved_by: getPreApproved.employee
                ? `${getPreApproved.employee.first_name} ${getPreApproved.employee.middle_name} ${getPreApproved.employee.last_name}`
                : '',
              department: employee.position
                ? employee.position.team
                  ? employee.position.team.department
                    ? employee.position.team.department.name
                    : ''
                  : ''
                : '',
              team: employee.position
                ? employee.position.team
                  ? employee.position.team.name
                  : ''
                : ''
            })
          );
        })
      );
    } else {
      state.approved = [];
    }

    if (Array.isArray(data.listDepartments)) {
      data.listDepartments = data.listDepartments.filter(department => department.status === 'Active');
      state.departments = data.listDepartments.map(
        department => department.name
      );
    } else {
      state.departments = [];
    }

    if (Array.isArray(data.listRequestTypes)) {
      data.listRequestTypes = data.listRequestTypes.filter(
        requestType => requestType.status === 'Active'
      );
      state.requestTypes = data.listRequestTypes.map(
        requestType => requestType.name
      );
    } else {
      state.requestTypes = [];
    }
  },
  SET_DISAPPROVED_REQUESTS(state, data) {
    state.disapproved = [];
    if (Array.isArray(data.listGatePass)) {
      data.listGatePass.map(employee =>
        employee.gate_passes.map(gate_pass => {
          let timeout = moment(
            `${gate_pass.request_time_out} ${gate_pass.request_date}`,
            'hh:mm a'
          ).format('hh:mm a');

          let timein;
          if (gate_pass.request_time_in) {
            timein = moment(
              `${gate_pass.request_time_in} ${gate_pass.request_date}`,
              'hh:mm a'
            ).format('hh:mm a');
          }

          const filterByDisapproved = gate_pass.gate_pass_status_logs.filter(
            log => log.status === 'Disapproved'
          );
          const getDisapproved = filterByDisapproved.reduce((prev, current) => {
            if (+current.id > +prev.id) {
              return current;
            } else {
              return prev;
            }
          });

          state.disapproved.unshift(
            (gate_pass.gate_pass = {
              id: gate_pass.id,
              name: {
                name: employee.last_name + ', ' + employee.first_name,
                id: employee.id,
                is_offsite: employee.is_offsite
              },
              request_type: gate_pass.request_type.name,
              reason:
                gate_pass.reason.length > 30
                  ? `${gate_pass.reason.substr(0, 30)}...`
                  : gate_pass.reason,
              time: timein ? timeout + ' - ' + timein : timeout,
              date: moment(gate_pass.request_date).format('MM/DD/YYYY'),
              disapproved_by: getDisapproved.employee
                ? `${getDisapproved.employee.first_name} ${getDisapproved.employee.middle_name} ${getDisapproved.employee.last_name}`
                : '',
              department: employee.position
                ? employee.position.team
                  ? employee.position.team.department
                    ? employee.position.team.department.name
                    : ''
                  : ''
                : '',
              team: employee.position
                ? employee.position.team
                  ? employee.position.team.name
                  : ''
                : '',
              note: gate_pass.note ? gate_pass.note : ""
            })
          );
        })
      );
    }

    if (Array.isArray(data.listDepartments)) {
      data.listDepartments = data.listDepartments.filter(department => department.status === 'Active');
      state.departments = data.listDepartments.map(
        department => department.name
      );
    } else {
      state.departments = [];
    }

    if (Array.isArray(data.listRequestTypes)) {
      data.listRequestTypes = data.listRequestTypes.filter(
        requestType => requestType.status === 'Active'
      );
      state.requestTypes = data.listRequestTypes.map(
        requestType => requestType.name
      );
    } else {
      state.requestTypes = [];
    }
  },
  CLEAR_REQUESTS(state) {
    (state.pending = []),
      (state.approved = []),
      (state.disapproved = []),
      (state.requestType = []),
      (state.departments = []);
    state.pendingCount = 0;
  }
};

