# LAYOUTS

> Sets the layout of your web application

## How to use


**Example**
```javascript

// ./layouts/dashboard.vue
<template>
    <div>
        <nuxt/>
    </div>
</template>

<script>
export default {
    // Your Javascript code here.
}
</script>

<style>
/* Your css here */
</style>
```

In your [Pages](https://bitbucket.org/bfiitteam/app_egps/src/master/pages/) Folder.
use the `layout` property.

```javascript

<template>
<div>
<!-- Your html code here -->
</div>
</template>

<script>
export default {

    //Add layout to your page
    layout:'dashboard'
}


</script>

```