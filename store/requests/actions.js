import axios from 'axios';
import { isArray } from 'util';
export default {
  async FETCH_PENDING_REQUESTS({ commit }, { dateFrom, dateTo }) {
    const employee = JSON.parse(localStorage.employee);
    const mapRequests = employee.modules.find(moduleEmp => moduleEmp.description === "Request")
    const mapActionRequests = mapRequests.actions.find(action => action.description === "View requests")

    if (mapActionRequests) {
      await axios({
        method: 'POST',
        url: `${this.$axios.defaults.baseURL}/requests/viewPending/${
          employee.roleParam
          }`,
        data: {
          employee_id: employee.id,
          role: employee.role,
          modules: employee.modules,
          request_date_to: dateTo,
          request_date_from: dateFrom
        },
        headers: {
          authorization: 'Bearer ' + employee.token
        }
      })
        .then(res => {
          commit('SET_PENDING_REQUESTS', res.data);
        })
        .catch(e => { });
    }
  },
  async FETCH_APPROVED_REQUESTS({ commit }, { dateFrom, dateTo }) {
    const employee = JSON.parse(localStorage.employee);
    // const modules = decrypt(employee.modules);
    await axios({
      method: 'POST',
      url: `${this.$axios.defaults.baseURL}/requests/viewApproved/${
        employee.roleParam
        }`,
      data: {
        employee_id: employee.id,
        role: employee.role,
        modules: employee.modules,
        request_date_to: dateTo,
        request_date_from: dateFrom
      },
      headers: {
        authorization: 'Bearer ' + employee.token
      }
    })
      .then(res => {
        commit('SET_APPROVED_REQUESTS', res.data);
      })
      .catch(e => { });
  },
  async FETCH_PENDING_REQUESTS_COUNT({ commit }, { dateFrom, dateTo }) {
    const employee = JSON.parse(localStorage.employee);
    const mapRequests = employee.modules.find(moduleEmp => moduleEmp.description === "Request")
    const mapActionRequests = mapRequests.actions.find(action => action.description === "View requests")
    if (mapActionRequests) {
      await axios({
        method: 'POST',
        url: `${this.$axios.defaults.baseURL}/requests/viewPending/${
          employee.roleParam
          }`,
        data: {
          employee_id: employee.id,
          role: employee.role,
          modules: employee.modules,
          request_date_to: dateTo,
          request_date_from: dateFrom,

        },
        headers: {
          authorization: 'Bearer ' + employee.token
        }
      })
        .then(res => {
          if (isArray(res.data.listGatePass)) {
            commit('SET_PENDING_COUNT', res.data.listGatePass);
          } else {

            commit('SET_PENDING_COUNT', res.data.listGatePass);
          }
        })
        .catch(e => { });
    }
  },
  async FETCH_APPROVED_REQUESTS({ commit }, { dateFrom, dateTo }) {
    const employee = JSON.parse(localStorage.employee);
    const mapRequests = employee.modules.find(moduleEmp => moduleEmp.description === "Request")
    const mapActionRequests = mapRequests.actions.find(action => action.description === "View requests")
    if (mapActionRequests) {
      await axios({
        method: 'POST',
        url: `${this.$axios.defaults.baseURL}/requests/viewApproved/${
          employee.roleParam
          }`,
        data: {
          employee_id: employee.id,
          role: employee.role,
          modules: employee.modules,
          request_date_to: dateTo,
          request_date_from: dateFrom
        },
        headers: {
          authorization: 'Bearer ' + employee.token
        }
      })
        .then(res => {
          commit('SET_APPROVED_REQUESTS', res.data);
        })
        .catch(e => { });
    }
  },
  async FETCH_DISAPPROVED_REQUESTS({ commit }, { dateFrom, dateTo }) {
    const employee = JSON.parse(localStorage.employee); const mapRequests = employee.modules.find(moduleEmp => moduleEmp.description === "Request")
    const mapActionRequests = mapRequests.actions.find(action => action.description === "View requests")
    if (mapActionRequests) {

      await axios({
        method: 'POST',
        url: `${this.$axios.defaults.baseURL}/requests/viewDisapproved/${
          employee.roleParam
          }`,
        data: {
          employee_id: employee.id,
          role: employee.role,
          modules: employee.modules,
          request_date_to: dateTo,
          request_date_from: dateFrom
        },
        headers: {
          authorization: 'Bearer ' + employee.token
        }
      })
        .then(res => {
          commit('SET_DISAPPROVED_REQUESTS', res.data);
        })
        .catch(e => commit('CLEAR_REQUESTS'));
    }
  }
};
