import axios from 'axios'
export default{

    async FETCH_TIME_IN({commit}){
        const employee = JSON.parse(localStorage.employee);
        const securityAccess = employee.modules.find(
          gatepass => gatepass.description === "Gate Pass"
        );
        const viewTimeIn = securityAccess.actions.find(
          timeIn => timeIn.description === "View time out gate pass"
        );
        if (viewTimeIn) {
          await axios({
            method: "POST",
            url: `${this.$axios.defaults.baseURL}/securityguard/viewTimeOut`,
            data: {
              employee_id: employee.id,
              modules: employee.modules
            },
            headers: {
              authorization: "Bearer " + employee.token
            }
          }).then(response => {
              commit("SET_TIME_IN",response);
          }).catch(error => {
                commit("SET_TIME_IN","No employees found")
          })
        }
    },
    async FETCH_TIME_OUT({commit}){
        const employee = JSON.parse(localStorage.employee);
        const securityAccess = employee.modules.find(
          gatepass => gatepass.description === "Gate Pass"
        );
        const viewTimeIn = securityAccess.actions.find(
          timeIn => timeIn.description === "View approved gate pass"
        );
        if (viewTimeIn) {
          await axios({
            method: "POST",
            url: `${this.$axios.defaults.baseURL}/securityguard/viewApproved`,
            data: {
              employee_id: employee.id,
              modules: employee.modules
            },
            headers: {
              authorization: "Bearer " + employee.token
            }
          }).then(response => {
              commit("SET_TIME_OUT",response);
          }).catch(error => {
              
                commit("SET_TIME_OUT",error)
          })
        }
    }
}