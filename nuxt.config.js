import colors from 'vuetify/es5/util/colors'
import webpack from 'webpack';
export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: "en",

    },

    titleTemplate: '%s',
    title: process.env.npm_package_description || '',
    meta: [

      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
      ,
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#005C220' },
  /*
  ** Global CSS
  */
  css: [

  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    'plugins/main',
    'plugins/papa-parse',

  ],
  // middleware: ['auth'],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/vuetify',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    'bootstrap-vue/nuxt',


  ],
  /**
   * Auth Settings
   */
  auth: {
  },
  router: {
  },
  // Change host server
  // server:{
  //   host:'172.16.4.45',
  //   port:'3001'
  // },
  bootstrapVue: {
    componentPlugins: [
      'ProgressPlugin',
      'AlertPlugin',
      'LayoutPlugin',
      'FormPlugin',
      'FormCheckboxPlugin',
      'FormInputPlugin',
      'FormGroupPlugin',
      'ToastPlugin',
      'ModalPlugin',
      'BadgePlugin',
      'ButtonPlugin',
      'CardPlugin',
      'CollapsePlugin',
      'DropdownPlugin',
      'FormFilePlugin',
      'FormSelectPlugin',
      'FormTextareaPlugin',
      'ImagePlugin',
      'InputGroupPlugin',
      'LinkPlugin',
      'ModalPlugin',
      'NavPlugin',
      'NavbarPlugin',
      'PaginationPlugin',
      'SpinnerPlugin',
      'TablePlugin',
      'TabsPlugin',
      'ToastPlugin',
      'PopoverPlugin',
      'BreadcrumbPlugin'
    ],
  },

  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    //DEV URL: 'http://api.jltechsol.com:3000'
    ///DEV URL:http://192.168.34.125:8030
    //SQA URL: 'http://api2.jltechsol.com:3000'
    //SQA URL: http://192.168.34.125:8020
    //UAT URL: 'http://192.168.34.125:3000' 
    baseURL: 'http://172.16.4.182:4002'
  },

  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    theme: {
      primary: colors.blue.darken2,
      accent: colors.grey.darken3,
      secondary: colors.amber.darken3,
      info: colors.teal.lighten1,
      warning: colors.amber.base,
      error: colors.deepOrange.accent4,
      success: colors.green.accent3
    },

  }, env: {
    WS_URL: 'http://localhost:5000'
  },

  // watchQuery: ['/'],               // Enable this feature for security guard module
  /*
  ** Build configuration
  */
  build: {
    extractCSS: process.env.NODE_ENV !== 'development',
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }

    },
    plugins: [
      new webpack.ProvidePlugin({
        // "moment": "moment",
        "bootstrap-vue": "bootstrap-vue",
        "vuetify": "vuetify",
        // "bootstrap":"bootstrap",
      })
    ]
  }
}
