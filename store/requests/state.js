export default () => ({
    pending:[],
    pendingWithDeduction: [],
    approved:[],
    disapproved:[],
    departments:[],
    requestTypes:[],
    pendingCount:0,
})