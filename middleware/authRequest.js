export default function ({ redirect }) {
    let employee;
    if (localStorage.employee) {
        employee = JSON.parse(localStorage.employee)
        const sec = employee.roleParam === 'sec'
        if (sec) {
            redirect('/monitor-dashboard')
        }
        const mapRequest = employee.modules.find(
            element => element.description === "Request");
        const canViewRequest = mapRequest.actions.find(
            action => action.description === "View requests");

        if ((localStorage.token && !canViewRequest) || sec) {
            redirect('/dashboard')
        }

    } else {
        redirect('/login')
    }

}