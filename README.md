# Employee Gate Pass System

An automated Gate pass system for the KCC Corporate Office.

### NOTICE: Hot reload may not support to specific browser/IPs being used.

> Hot reload feature was disabled by the network administrator, thus it requires to hit `refresh` everytime we change our UI.

## Build Setup

```bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
# If localhost:3000 is being used. it will generate another port to access the EGPS.
# this really depends on the browser if hot reload is supported
$ npm run serve

# the default `localhost:3000` has been set for API usage only.
# go to http://localhost:3000 in your browser

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

```bash
# To run all unit test
$ npm run test

# To run a specific file for unit test

$ npm run test test/login.test.js # this will run login.test.js file
```

### Folder Structure.

> The EGPS has a folder structure that can be modified accordingly. This helps us maintain the codebase structure and have segregate the purposes of the intent of the file.

#### Components

Under the [Components](https://bitbucket.org/bfiitteam/app_egps/src/master/components/) Folder.
Every modules can be found here. Read the documentation from the link [here](https://bitbucket.org/bfiitteam/app_egps/src/master/components/)

#### Layouts

Under the [Layouts](https://bitbucket.org/bfiitteam/app_egps/src/master/layouts/) Folder.
The usage of this folder is to have a universal layout for your application. so that it won't repeat the same codebase to be used for every page you want to create. If you want to add another layout. be sure to add the `<nuxt />` tag in the `.vue` file.

#### Pages

Under the [Pages](https://bitbucket.org/bfiitteam/app_egps/src/master/pages/) Folder.
You can add additional route to the EGPS by simply creating a file under the pages folder.
The framework itself will auto generate the route you created in that folder.

#### Store(Advance Usage)

Under the [Store](https://bitbucket.org/bfiitteam/app_egps/src/master/store) Folder.
This is where the [Vuex](https://vuex.vuejs.org) comes. This is your state management folder that can be reuse to access business logic anywhere in the `.vue` file.

#### Middleware

Under the [Middleware](https://bitbucket.org/bfiitteam/app_egps/src/master/middleware) Folder.
This Folder contains your authentication for the EGPS. The purpose of the middleware is to ensure that the user can only access to a certain pages and not able to view the content of the application.


#### Plugins

Under the [Plugins](https://bitbucket.org/bfiitteam/app_egps/src/master/plugins) Folder.
This is where you register your 3rd party javascript/vue dependencies globally.

#### Asssets

Under the [Assets](https://bitbucket.org/bfiitteam/app_egps/src/master/assets) Folder,
You can store your images, fonts or svgs. Webpack will automatically compress your files in the assets folder.

#### Test(Optional)

Under the [Test](https://bitbucket.org/bfiitteam/app_egps/src/master/test) Folder.
You can test your business logic code and the appearance of the UI if it exists or the UI gets updated accordingly.

### Code coverage.

```javascript
/**
 * Open `jest.config.js` in the main directory
 *
 */
module.exports = {
  collectCoverage: true // Set this to false to disable code coverage
};
```

## Tree shaking

Please do take note if you want to add unique bootstrap component from [bootstrap-vuejs](http://bootstrap-vuejs.org), Add the component to `nuxt.config.js` under the **bootstrapVue** object. The application bundle size has been reduce to 10% without tree shaking.
