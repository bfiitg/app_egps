import { mount, shallowMount, createLocalVue } from '@vue/test-utils';
import BootstrapVue from 'bootstrap-vue';
import ChangePassword from '@/components/Login/ChangePassword';


const localVue = createLocalVue();

localVue.use(BootstrapVue)

describe('Change Password Component', () => {


    const wrapper = mount(ChangePassword, {
        localVue,
        propsData: {
            employee_id: "154151100",

        }
    })
    test('is a Vue Instance', () => {
        expect(wrapper.isVueInstance()).toBeTruthy();

    })
    test('of ID has a length of greater than 3', () => {
        expect(wrapper.props().employee_id.length).toBeGreaterThan(3)
    })
    test('of ID has a length of less than 20', () => {
        expect(wrapper.props().employee_id.length).toBeLessThan(20)
    })
    test('of ID to follow standard number `154`', () => {
        expect(wrapper.props().employee_id).toMatch(/154/)
    })
    test('of login button is disabled', () => {
        wrapper.find('button').trigger('click')
        expect(wrapper.attributes('id')).toBe('update-password')
        
    })

})