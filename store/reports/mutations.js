import moment from 'moment';
import { isArray } from 'util';

export default {
  SET_VERIFIED_REPORTS(state, data) {
    if (isArray(data.listGatePass)) {
      this.state.FinalData = [];
      data.listGatePass.map(employee =>
        employee.gate_passes.map(gatepass => {
          let time_out = null;
          let time_in = null;
          let approval_time = null;

          let requested_time_in,
            requested_time_out = null;
          if (gatepass.actual_time_out) {
            time_out = moment(
              `${gatepass.actual_time_out} ${gatepass.request_date}`,
              "hh:mm a"
            ).format("hh:mm a");
          }
          if (gatepass.approval_time) {
            approval_time = moment(
              `${gatepass.approval_time} ${gatepass.request_date}`,
              "hh:mm a"
            ).format("hh:mm a");
          }

          if (gatepass.actual_time_in) {
            time_in = moment(
              `${gatepass.actual_time_in} ${gatepass.request_date}`,
              "hh:mm a"
            ).format("hh:mm a");
          }

          if (gatepass.request_time_in) {
            requested_time_in = moment(
              `${gatepass.request_time_in} ${gatepass.request_date}`,
              "HH:mm a"
            ).format("hh:mm a");
          }
          if (gatepass.request_time_out) {
            requested_time_out = moment(
              `${gatepass.request_time_out} ${gatepass.request_date}`,
              "hh:mm a"
            ).format("hh:mm a");
          }
          const filterByVerified = gatepass.gate_pass_status_logs.filter(
            log => log.status === "Verified"
          );
          const getVerified = filterByVerified.reduce((prev, current) => {
            if (+current.id > +prev.id) {
              return current;
            } else {
              return prev;
            }
          });

          state.FinalData.push({
            employee_id: employee.id,
            id: gatepass.id,
            emp_name: {
              last_name: employee.last_name,
              middle_name: employee.middle_name,
              first_name: employee.first_name
            },
            name: {
              name: `${employee.last_name} ${
                employee.middle_name ? employee.middle_name : ""
                } ${employee.first_name}`,
              id: employee.id
            },
            request_type: gatepass.request_type.name,
            requested_time: gatepass.request_time_in
              ? `${requested_time_out} - ${requested_time_in}`
              : requested_time_out,

            time_in,
            time_out,
            approval_time,
            status: gatepass.status,
            date: moment(gatepass.request_date).format("MM/DD/YYYY"),
            verified_by: ` ${getVerified.employee.last_name} ${getVerified.employee.middle_name} ${getVerified.employee.first_name} `,
            elapsed_time:
              gatepass.elapsed_time > 0
                ? gatepass.elapsed_time + " Hour(s)"
                : "",
            department: employee.position
              ? employee.position.team
                ? employee.position.team.department
                  ? employee.position.team.department.name
                  : ""
                : ""
              : "",
            team: employee.position
              ? employee.position.team
                ? employee.position.team.name
                : ""
              : ""
          });
        })
      );
    } else {
      state.FinalData = [];
    }
    if (isArray(data.listDepartments)) {
      state.Departments = [];
      data.listDepartments = data.listDepartments.filter(
        department => department.status === "Active"
      );
      state.Departments = data.listDepartments;
    }
    if (isArray(data.listRequestTypes)) {
      state.Request_type = [];
      data.listRequestTypes = data.listRequestTypes.filter(
        requestType => requestType.status === "Active"
      );
      state.Request_type = data.listRequestTypes;
    }
  }
}