export default {
    getPendingLists(state) {
        return state.pending
    },
    getPendingWithDeductionsLists(state) {
        return state.pendingWithDeduction
    },
    getApprovedLists(state) {
        return state.approved
    },
    getDisapprovedLists(state) {
        return state.disapproved
    },
    getAllDepartments(state) {
        return state.departments
    },
    getAllRequestTypes(state) {
        return state.requestTypes;
    },
    getPendingCount(state){
        return state.pendingCount
    }
}